# Setup fzf
# ---------
if [[ ! "$PATH" == */home/adjco/Downloads/Software/fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/adjco/Downloads/Software/fzf/bin"
fi

# Auto-completion
# ---------------
source "/home/adjco/Downloads/Software/fzf/shell/completion.bash"

# Key bindings
# ------------
source "/home/adjco/Downloads/Software/fzf/shell/key-bindings.bash"
