# Modified commands
alias bashrc='nvim ~/.bashrc'
alias cpan='cpanm'
alias df='duf -theme ansi'
# alias diff='colordiff'              # requires colordiff package
alias du='du -c -h'
alias grep='grep --color=auto'
alias jsdoc='jsdoc -c ./jsdoc/conf.json'
alias joe='jstar'
alias mkdir='mkdir -p -v'
alias more='less'
# alias nano='nano -w'
alias path='echo $PATH'
alias ping='ping -c 5'
alias qemu='qemu-system-x86_64'
alias top='htop'
alias tmuxinator='tmuxinator.ruby2.5'
#--------------------------------
# For tmux
# alias vi='TERM="" nvim'
#alias vim='TERM="" nvim'
#--------------------------------
alias tree-sitter='tree-sitter-linux-x64'
#--------------------------------------------------------
# For the updated grep - https://github.com/Genivia/ugrep
alias uq='ug -Q'
alias ux='ug -U --hexdump'
alias uz='ug -z'
alias ugit='ug --ignore-files'
alias grep='ugrep -G'
alias egrep='ugrep -E'
alias fgrep='ugrep -F'
alias pgrep='ugrep -P'
alias xgrep='ugrep -U --hexdump'
alias zgrep='ugrep -zG'
alias zegrep='ugrep -zE'
alias zfgrep='ugrep -zF'
alias zpgrep='ugrep -zP'
alias zxgrep='ugrep -zU --hexdump'
alias xdump='ugrep -X ""'
#--------------------------------------------------------
# For xterm
alias vi='nvim'
#--------------------------------

# New commands
# alias da='date "+%A, %B %d, %Y [%T]"'
# alias du1='du --max-depth=1'
alias cls='clear'
alias err='echo $?'
alias h='history'
alias hist='history | grep'         # requires an argument
alias openports='ss --all --numeric --processes --ipv4 --ipv6'
# alias pgg='ps -Af | grep'           # requires an argument
alias ..='cd ..'

# fasd shortcuts
alias a='fasd -a'        # any
alias s='fasd -si'       # show / search / select
alias d='fasd -d'        # directory
alias f='fasd -f'        # file
alias sd='fasd -sid'     # interactive directory selection
alias sf='fasd -sif'     # interactive file selection
alias j='fasd_cd -d'  # cd, same functionality as j in autojump
#alias zz='fasd_cd -d -i' # cd with interactive selection

# Privileged access
if (( UID != 0 )); then
    alias sudo='sudo '
    alias scat='sudo cat'
    alias svim='sudoedit'
    alias root='sudo -i'
    alias reboot='sudo systemctl reboot'
    alias poweroff='sudo systemctl poweroff'
    alias update='sudo pacman -Su'
    alias netctl='sudo netctl'
fi

# ls
alias ls='ls -hF --color=auto'
alias lr='ls -R'                    # recursive ls
alias ll='ls -l'
alias la='ll -A'
alias lx='ll -BX'                   # sort by extension
alias lz='ll -rS'                   # sort by size
alias lt='ll -rt'                   # sort by date
alias lm='la | more'

# Safety features
alias cp='cp -i'
alias mv='mv -i'
# alias rm='rm -I'                    # 'rm -i' prompts for every file
# safer alternative w/ timeout, not stored in history
# alias rm=' timeout 3 rm -Iv --one-file-system'
# alias ln='ln -i'
# alias chown='chown --preserve-root'
# alias chmod='chmod --preserve-root'
# alias chgrp='chgrp --preserve-root'
alias cls=' echo -ne "\033c"'       # clear screen for real (it does not work in Terminology)

# Make Bash error tolerant
alias :q=' exit'
alias :Q=' exit'
alias :x=' exit'
alias cd..='cd ..'
