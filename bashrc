# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


[ -f ~/.ansiicolours ] && source ~/.ansiicolours

# Include gnu modula-2 compilers
# PATH="/opt/gm2/bin/:$PATH:"

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim

# Comment out the next instruction for tmux
export TERM=screen-256color
export COLORTERM=24bit

# Set the width of man pages and add colour.
export MANWIDTH=80
[ -f ~/.colourman ] && source ~/.colourman

# go workspaces
export GOROOT=/usr/local/go
export GOPATH=~/Projects/go:
export PATH=$PATH:/usr/local/go/bin

# Enable qt5rc in superuser mode
export QT_QPA_PLATFORMTHEME=qt5ct

# Active /usr/local/lib libraries
export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/lib64/:$LD_LIBRARY_PATH

# Set location of Z88DK executable and libraries
export PATH=${PATH}:${HOME}/Downloads/Emulation/Z80/z88dk/bin
export ZCCCFG=${HOME}/Downloads/Emulation/Z80/z88dk/lib/config

# Set path and environment for z88dx
#export PATH=${PATH}:${HOME}/Downloads/Emulation/z88dk/bin
#export ZCCCFG=${HOME}/Downloads/Emulation/z88dk/lib/config

# Set colorful PS1 with SSH connection warning
# if [[ $(who am i) =~ \([-a-zA-Z0-9\.]+\)$ ]] ; then
if [ -n "$SSH_CLIENT" ]; then
    remote_ssh=" ssh "
else
    remote_ssh=""
fi
if [[ ${EUID} == 0 ]] ; then
    PS1='\e[41m$remote_ssh\e[0m \[\033[01;31m\]\h\[\033[01;36m\] \w\n\$\[\033[00m\] '
else
    PS1='\e[41m$remote_ssh\e[0m \[\033[01;32m\]\u@\h\[\033[01;36m\] \w\n\$\[\033[00m\] '
fi

#Enable autojump - replaced by fasd
eval "$(fasd --init auto)"

# and dirb
[[ -s /home/adjco/bin/dirb/dirb.sh ]] && source /home/adjco/bin/dirb/dirb.sh

# and the Bash Line Editor script
source ~/.local/share/blesh/ble.sh

# append to the history file, don't overwrite it
shopt -s histappend

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoredups:ignorespace:erasedups

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTTIMEFORMAT='%F %T '
HISTSIZE=10000
HISTFILESIZE=20000
export HISTIGNORE="?":"??"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Disable Ctrl-S, Ctrl-Q flow control
stty -ixon

# enable bash completion in interactive shells
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

test -s ~/.bash_aliases && . ~/.bash_aliases || true

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export AVR_ROOT=/usr/avr/sys-root

# fzf settings
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -f ~/.fzf-extras/fzf-extras.sh ] && source ~/.fzf-extras/fzf-extras.sh

# nvm settings
export PATH=$PATH:~/.npm-global/bin
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" 

# Local perlbrew
export PERLBREW_ROOT=~/bin/perl
[ -f ~/bin/perl/etc/bashrc ] && source ~/bin/perl/etc/bashrc

# Add cgi-bin and local perl to perl module path
export PERL5LIB=~/www/cgi-bin

export GTK_BACKEND="x11"

# add cross-compiler to path
export PATH=$PATH:/opt/cross/bin

#fortune -e |cowsay -f tux

export PID=$$
count=`ps -e |grep tmux | wc -l`

if [ "$CONTEXT" == 'Development' ]; then
    if [ -z "$TMUX" ]; then
        exec tmux new -s Development_$count
    fi
elif [ "$CONTEXT" == 'Context' ]; then
    if [ -z "$TMUX" ]; then
        exec tmux new -s Context_$count
    fi
fi


